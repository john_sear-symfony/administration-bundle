# Administration Bundle
(c) 2020 by [John_Sear](https://bitbucket.org/john_sear-symfony/administration-bundle/)

## Administration

This is an Administration Bundle for your Symfony application.  
Be sure you are using symfony version 4.4.

> This Bundle is still in Development. Some things can be broken ;-)