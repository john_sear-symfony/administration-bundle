<?php declare(strict_types=1);

namespace JohnSear\JspAdministrationBundle;

use JohnSear\JspAdministrationBundle\DependencyInjection\JspAdministrationExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class JspAdministrationBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new JspAdministrationExtension();
    }
}
