<?php declare(strict_types=1);

namespace JohnSear\JspAdministrationBundle\Controller\Api\v1\AccessControl;

use Doctrine\ORM\EntityManager;
use JohnSear\JspApiBundle\DependencyInjection\Controller\AbstractApiController as ApiController;
use JohnSear\JspUserBundle\Entity\User;
use JohnSear\JspUserBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/access-control/user", name="access_control_user_")
 * @IsGranted("IS_AUTHENTICATED_FULLY")
 */
class UserController extends ApiController
{
    /**
     * @Route("/", name="index")
     * @Route("/list", name="list")
     */
    public function list(): Response
    {
        /** @var EntityManager $em */
        $em = $this->get('doctrine')->getManager();

        /** @var UserRepository $userRepository */
        $userRepository = $em->getRepository(User::class);

        return $this->json($userRepository->findAll());
    }
}
