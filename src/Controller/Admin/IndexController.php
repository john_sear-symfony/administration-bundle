<?php declare(strict_types=1);

namespace JohnSear\JspAdministrationBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class IndexController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(): Response
    {
        return new Response('<h1>Hello World</h1>');
    }
}
