<?php declare(strict_types=1);

namespace JohnSear\JspAdministrationBundle\DependencyInjection;

use JohnSear\JspAdministrationBundle\Controller\Api\v1\AccessControl\UserController;
use JohnSear\JspAdministrationBundle\Controller\Admin\IndexController;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\BadMethodCallException;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class JspAdministrationExtension extends Extension
{
    /** @var ContainerBuilder */
    private $container;

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $this->container = $container;

        $this->registerController();
    }

    private function registerController(): void
    {
        $this->registerAdminController();
        $this->registerApiController();
    }

    private function registerAdminController(): void
    {
        $this->registerIndexController();
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerIndexController(): void
    {
        $definition = new Definition(IndexController::class, []);
        $this->container->setDefinition('jsp.administration.admin.index_controller', $definition)
            ->addTag('controller.service_arguments')
            ->setPublic(true);
        $this->container->setAlias(IndexController::class, 'jsp.administration.admin.index_controller')
            ->setPublic(true);
    }

    private function registerApiController(): void
    {
        $this->registerApiV1Controller();
    }

    private function registerApiV1Controller(): void
    {
        $this->registerApiV1AccessControlUserController();
    }

    /**
     * @throws BadMethodCallException
     * @throws InvalidArgumentException
     */
    private function registerApiV1AccessControlUserController(): void
    {
        $definition = new Definition(UserController::class, []);
        $this->container->setDefinition('jsp.administration.api.v1.access_control.user_controller', $definition)
            ->addTag('controller.service_arguments')
            ->setPublic(true);
        $this->container->setAlias(UserController::class, 'jsp.administration.api.v1.access_control.user_controller')
            ->setPublic(true);
    }
}
